<?php
//INIT VARS
	//echo $lang; exit();
	date_default_timezone_set("America/Costa_Rica"); 
	$doTimestampUrl = false;
	$urlRW = true;
	$defTitle = "Punta Cana Poker Classic";
	$defDescription = "";
	$defDescription = ($lang=="en"? "Punta Cana Poker Classic is a series of major live poker tournaments throughout Central and South America, sponsored by the Winning Poker Network.":"Punta Cana Poker Classic es una serie de importantes torneos de poker en vivo disputados por toda América Central y del Sur y patrocinados por Winning Poker Network.");
	$defKeywords = "";
	$isCompanyUser = isCompanyUser();
	
//CREATE MENUS
	$hMenu = fetchMenu_with_description();

	if (!array_key_exists("items", $hMenu))  $hMenu["items"] = array();
	array_unshift($hMenu["items"],array("name_en" => "Home", "name_es" => "Inicio", "url" => "index.php", "urlRW_en" => "/?lang=en", "urlRW_es" => "/?lang=es", "cls" => "newpcpc_menu", "id" => NULL, "items" => NULL));
	$hMenu["name_en"] = "Home";
	$hMenu["urlRW_en"] = "/?lang=en";
	$hMenu["name_es"] = "Inicio";
	$hMenu["urlRW_es"] = "/?lang=es";
	$hMenu["id"] = "";

	$fMenu = array("name_en" => "Homepage", "url" => "index.php", "urlRW_en" => "/", "cls" => NULL, "id" => "main",
		"items" => array(
			findLinkInMenu($hMenu, ($lang=="en"?"Home":"Inicio")),
			findLinkInMenu($hMenu, ($lang=="en"?"About":"Acerca")),			
			findLinkInMenu($hMenu, ($lang=="en"?"Tournament":"Torneo")),
			findLinkInMenu($hMenu, ($lang=="en"?"Media":"Medios")),
			findLinkInMenu($hMenu, ($lang=="en"?"The Package":"El Paquete")),
			findLinkInMenu($hMenu, ($lang=="en"?"Venue":"Hotel")),
			findLinkInMenu($hMenu, ($lang=="en"?"Sponsors":"Patrocinan")),
			findLinkInMenu($hMenu, ($lang=="en"?"Contact":"Contacto")),
			findLinkInMenu($hMenu, ($lang=="en"?"Book Now":"Reservar"))
			
		)
	);

	$errors = array();
	if (isset($_POST["email"])){
		session_start();
	    if(!isset($_SESSION['email_sent'])) {
			sendEmail();
		}else{
			array_push($errors, 'Your request has already been processed');
		}
		// getEmailResultHTML("Thanks for your message, we will contact you shortly");
		 print_r($errors);
	}

//CUSTOM FUNCTIONS
	/**
	 * Generate a widget with latest 8 blog posts
	 * @param  integer $start
	 * @param  integer $count
	 * @param  integer  $sid
	 * @return string
	 */
	function getWidgetBlogPosts($start=0, $count=8, $maxl=200){
		$widget="";
		$posts=getBlogPosts($start, $count, NULL, NULL, NULL, NULL, array(), NULL, TRUE);
		foreach($posts as $i => $post){
			$content = $post["post_content"];
			$ts = strtotime($post["post_created"]);
			$pos = stripos($content, "</p>");
			$widget .= '<div class="widgetBlogPost" >';
			$widget .= postImageFormat($post, NULL, NULL, 190, FALSE);
			$widget .= '<h4>' . $post["post_title"] . '<time class="updated dtstamp" title="' . str_replace(' ', 'T', date("Y-m-d H:i:s", $ts)) . 'Z"><strong>' . date("M", $ts) . "</strong> <b>" . date("j", $ts) . "</b></time></h4>\r\n";
			//$widget .= '<p>'.getDescriptionFromContent($content, 400).'</p>';
			$widget .= '<div class="entry-summary"><p>' . getDescriptionFromContent(substr($content, 0, ($pos? $pos+4:$maxl)),$maxl, false, true) . '</p></div>';			
			$widget .= '<a class="readmore" href="/blog/' . date("Y/m", strtotime($post["post_created"])) . "/" .  $post["post_slug"] . '/">Read More</a>';
			$widget .= "</div>";
		}
		return $widget;
	}
	/**
	 * Return appropriate title
	 * @param  string $title
	 * @return string
	 */
	function returnTitle($title){
		if ($title == "Tournament Information"){
			return "Tournaments";
		}elseif ($title == "Poker Games"){
			return "How To Play Poker";
		}else{
			return $title;
		}
	}
	/**
	 * Get name of an item, convert to title if true
	 * @param  [type]  $item
	 * @param  boolean $convert
	 * @return string
	 */
	function getItemName($item, $convert=FALSE){
		global $lang;
		if (isset($item["name_".$lang])){
			$n = $item["name_".$lang];
		}elseif (isset($item["name_en"])){
			$n = $item["name_en"];
		}elseif(isset($item["name"])){
			$n = $item["name"];
		}else{
			$n = "Menu";
		}
		if ($convert) returnTitle($n);
		return $n;
	}


	/**
	 * Get name of an item, convert to title if true
	 * @param  [type]  $item
	 * @param  boolean $convert
	 * @return string
	 */
	function getItemTitleName($item){
		global $lang;
		//echo $lang;
		//print_r ($item);
		if (isset($item["name_".$lang])){
			$n = $item["name_".$lang];
			//echo $n;
		}elseif (isset($item["menuname_en"])){
			$n = $item["menuname_en"];
			//echo $n;
		}elseif(isset($item["menuname"])){
			$n = $item["menuname"];
			//echo $n;
		}/*else{
			$n = "Menu";
		}*/
		return $n;
	}
	/**
	 * [getCurrentPageInMenu description]
	 * @return [type]
	 */
	function getCurrentPageInMenu(){
		global $slug;
		return findLinkInMenu($hMenu, $slug); //$_SERVER['SCRIPT_NAME']
	}

	function translateUrl($lng){
		global $hMenu, $urlRW, $slug, $lang;
		$notinmenu=array('en' => '/book-now/', 'es' => '/reservar/');
		//print_r($notinmenu);
		$nquery = str_replace(array("&lang=".$lang,"lang=".$lang), array("",""), $_SERVER['QUERY_STRING']); if ($nquery!=="") $nquery = "?" . $nquery;
		print_r($nquery);

		$rquery = str_replace(array("&lang=".$lang,"lang=".$lang), array("&lang=".$lng,"lang=".$lng), $_SERVER['QUERY_STRING']); if ($rquery!=="") $rquery = "?" . $rquery;
		
		if (($item = findLinkInMenu($hMenu, $slug)) !== false){
			return pickLink($item, $lng) .  $nquery;
		}else if (($item = findLinkInMenu($hMenu, $slug . ($urlRW? "?":"&") . "lang=" . $lang)) !== false){
			return pickLink($item, $lng) .  $nquery;
		}else if ($notinmenu[$lang]==$slug) {
				return $notinmenu[$lng];

		}else return $url . $rquery;
	}
	
	/**
	 * Return string of image html code for specific post, used for blogs.
	 * 
	 * @param  [type]  $post
	 * @param  [type]  $title
	 * @param  integer $width
	 * @return string [HTML Code]
	 */
	function postImageFormat($post, $title=NULL, $titlelink=NULL, $height=150, $autohide=TRUE, $towidth=NULL){
		$img = "";
		if (isset($post["mediaitem_name"]) && $post["mediaitem_name"] !== ""){
			$w = round(($height/intval($post["mediaitem_height"]))*intval($post["mediaitem_width"]), 0);
			if ($towidth!==NULL && is_int($towidth) && $w < $towidth){
				$w = $towidth;
			}
		}else $w = NULL;
		if (!$autohide || ($autohide && $w!==NULL)){
			//$h = round(($width/intval($post["mediaitem_width"]))*intval($post["mediaitem_height"]), 0);
			$img .= '<div class="imgblock hmedia" style="' . ($w!==NULL? 'background-image:url(/includes/imgresize.php?url=../images/content/' . $post["mediaitem_name"] . '&width='.$w.');':'') . 'height:'.$height.'px;">';
			if ($title !== NULL){
				$img .= '<h3 style="margin-top:' . ($height-38) . 'px"><a href="' . $titlelink . '">' . $title . '</a></h3>';
			}else $img .= "&nbsp;";
			$img .= '</div>';
		}
		return $img;
	}	
	function generatePostSummary($post, $title=NULL, $titlelink=NULL, $related=array(), $imgtowidth=322){
		$poststr = "";
		$content = trim($post["post_content"]);
		$pos = stripos($content, "</p>");
		if (strlen($post["post_title"]) > 50){
			$maxl = 91;
		}elseif (strlen($post["post_title"]) > 30){
			$maxl = 184;
		}else $maxl = 276;
		$content = htmlspecialchars_decode(strip_tags(substr($content, 0, ($pos? $pos+4:$maxl)))); //, "<b><i><strong><em><span>");
		if (strlen($content) > $maxl) $content = trim(substr($content, 0, $maxl-3)) . "...";
		$poststr .= postImageFormat($post, $title, $titlelink, 150, FALSE, $imgtowidth);
		$poststr .= '<div class="entry"><h4><a href="/blog/' . date("Y/m", strtotime($post["post_created"])) . "/" .  $post["post_slug"] . '/">' . $post["post_title"] . '</a></h4><p class="entry-summary">' . $content . '</p></div>';
		if (isNotEmpty($title)){
			$poststr .= '<div id="blogSocialExt" class="addthis_sharing_toolbox" data-url="http://www.americascardroom.eu/blog/' . date("Y/m", strtotime($post["post_created"])) . "/" .  $post["post_slug"] . '/" data-title="'.$post["post_slug"].'"></div>';
			$poststr .= '<div class="entries"><ul>';
			if (count($related)){
				foreach ($related as $r => $rel){
					$poststr .= '<li><a href="/blog/' . date("Y/m", strtotime($rel["post_created"])) . "/" .  $rel["post_slug"] . '/">' . $rel['post_title'] . '</a></li>';
				}
			}
			$poststr .= '</ul></div>';
		}
		$poststr .= '<div class="entryfooter"><a class="commentcount" href="/blog/' . date("Y/m", strtotime($post["post_created"])) . "/" .  $post["post_slug"] . '/#disqus_thread"><!-- Comments (3) --></a><a class="readmore" href="/blog/' . date("Y/m", strtotime($post["post_created"])) . "/" .  $post["post_slug"] . '/">Read More</a></div>';
		return $poststr;
	}
	/**
	 * Add Captcha functionality to html.
	 * @param  [type] $content
	 * @return string [HTML Code]
	 */
	function captchaReplace($content){
		global $captchaPubKey;
		$before = '<input class="send"';
		if (stristr($content, $before) !== FALSE){
			return str_replace($before, '<h5>Drag arrow to the right to send message</h5><div class="QapTcha"></div>' . $before, $content);
		}else return $content;
	}
	/**
	 * Send email to support@americascardroom.eu. Uses POST variables.
	 * 
	 */
	function sendEmail(){
		global $errors, $captchaPrivKey;
		$from = "postman@americascardroom.eu"; 
		$fromname = "ACR.eu Website Support Form";
		$to = "info@puntacanapokerclassic.com";/*"support@americascardroom.eu"; "Esteban@imail.ag"; */
		$toname = $fromname;
		$subject = $fromname;
		$name = (isset($_POST["name"])? trim($_POST["name"]):"");
		$last_name = (isset($_POST["last_name"])? trim($_POST["last_name"]):"");
		$country = (isset($_POST["country"])? trim($_POST["country"]):"");
		$email = (isset($_POST["email"])? trim($_POST["email"]):"");
		$address = (isset($_POST["address"])? trim($_POST["address"]):"");
		$phone = (isset($_POST["phone"])? trim($_POST["phone"]):"");
		$checkin = (isset($_POST["checkin"])? trim($_POST["checkin"]):"");
		$checkout = (isset($_POST["checkout"])? trim($_POST["checkout"]):"");
		$rooms = (isset($_POST["rooms"])? trim($_POST["rooms"]):"");
		$adults = (isset($_POST["adults"])? trim($_POST["adults"]):"");
		$children = (isset($_POST["children"])? trim($_POST["children"]):"");
		$message = (isset($_POST["comments"])? trim($_POST["comments"]):"");
		// if(isset($_COOKIE['qaptcha_key']) && !empty($_COOKIE['qaptcha_key'])){
		// 	$qaptcha_key = $_COOKIE['qaptcha_key'];
		// 	$resp = (isset($_POST[''.$qaptcha_key.'']) && empty($_POST[''.$qaptcha_key.'']));
		// }else $resp = false;
		unset($_COOKIE["qaptcha_key"]); 
		setcookie("qaptcha_key", NULL, -1, '/', $_SERVER['HTTP_HOST']);
		if ($name == "" || $email == "" /*|| $message == ""*/){ 
			array_push($errors, "All fields are required.");
		// }elseif (!$resp && !isset($_GET['simple'])){
		// 	array_push($errors, 'Sorry! You failed to prove that you are human.');
		}else{
			require_once("includes/phpmailer521/class.phpmailer.php");
			$mail = new PHPMailer();
			$mail->IsSMTP();
			//$mail->SMTPAuth = false;
			$mail->SMTPDebug  = 0;
			//$mail->SMTPSecure = "ssl";
			$mail->Debugoutput = 'html';
			$mail->Host       = "mail.imail.ag";
			$mail->Port       = 25;
			$mail->SMTPAuth   = true;
			$mail->Username   = $from;
			$mail->Password   = "P0R9gv6b7235W25";
			$mail->AddReplyTo($email, $name);
			//$mail->IsSendmail();
			$mail->AddAddress($to, $toname);
			$mail->SetFrom($from, $fromname);
			$mail->Subject = $subject;
			$mail->IsHTML(false);
			$mail->Body = "NAME:\t" . $name . "\r\n" . 
			"LAST NAME:\t" . $last_name . "\r\n" . 
			"COUNTRY:\t" . $country . "\r\n" . 
			"ADDRESS:\t" . $address . "\r\n" .  
			"PHONE:\t" . $phone . "\r\n" . 
			"CHECKIN:\t" . $checkin . "\r\n" . 
			"CHECKOUT:\t" . $checkout . "\r\n" . 
			"ROOMS:\t" . $rooms . "\r\n" . 
			"ADULTS:\t" . $adults . "\r\n" .
			"CHILDREN:\t" . $children . "\r\n" .
			"EMAIL:\t" . $email . "\r\n" . 
			"MESSAGE:\t" . $message;
			if(!$mail->Send()) {
				// echo $mail->ErrorInfo;
				//array_push($errors, $mail->ErrorInfo);
				array_push($errors, 'Error sending email. Please send message to <a href="mailto:' . $to . '">' . $to . '</a>');
			}else{
				 $_SESSION['email_sent'] = "processed"; 
			}
		}
	}
	/**
	 * [getEmailResultHTML description]
	 * @param  string [HTML Code] $successMsg
	 * @return [type]
	 */
	function getEmailResultHTML($successMsg){
		global $errors;
		if (!count($errors)){
			return $successMsg;
		}else{
			return '<h5 class="warning">' . implode('<br/>', $errors) . '</h5>';
		}  
	}
	function getJSONTable(){
		$url="http://externalsvc.wpnetwork.eu/api/ExternalSvc/GetLeaderboardResults?numberOfBestTournaments=20&leaderboardId=1";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);// get the url contents
		$data = curl_exec($ch); // execute curl request
		curl_close($ch);
		//$xml = new SimpleXMLElement($data);
		return json_decode($data);
		
	}
	// function getLeaderboardTable(){
	// 	$data=getJSONTable();
	// 	$leaderboard = $data->LeaderboardResults;
	// 	$result ='<table style="width=100%"><thead><tr><td style="text-align: center;">';
	// 	$result .= 'Position</td>';
	// 	$result .= '<td style="text-align: center;">';
	// 	$result .= 'Nickname</td>';
	// 	$result .= '<td style="text-align: center;">';
	// 	$result .= 'Points</td></tr></thead><tbody>';
	// 	foreach ($leaderboard as $key => $value) {
	// 		$position = $value->Position;
	// 		$nickname = $value->Nickname;
	// 		$points = $value->Points;
	// 		$result .= '<tr><td style="text-align:center;">';
	// 		$result .= $position;
	// 		$result .= '</td><td style="text-align:center;">';
	// 		$result .= $nickname;
	// 		$result .= '</td><td style="text-align:center;">';
	// 		$result .= $points;
	// 		$result .= "</td></tr>";
	// 	}
	// 	$result .= "</tbody>";
	// 	$result .= "</table>";
	// 	return $result;
	// }
	// function secondsToTime($seconds) {
	// 	$dtF = new DateTime("@0");
	// 	$dtT = new DateTime("@$seconds");
 //  		return $dtF->diff($dtT)->format('%h:%i');
	// }

/**
	 * [preprocessPages description]
	 * @param  [type] $pages  [description]
	 * @param  [type] $root   [description]
	 * @param  [type] $ipages [description]
	 * @param  [type] $okey   [description]
	 * @return [type]         [description]
	 */
	function preprocessPages_pcpc($pages, $root, &$ipages, $okey){
		if (!array_key_exists($okey, $ipages)) $ipages[$okey] = array();
		$hs = explode("|", $root["page_name"]); $h1 = trim($hs[0]);
		if (count($hs) > 1){ $h2 = trim($hs[1]); }else $h2 = "";
		$ipages[$okey]["name_" . $root["page_lang"]] = $h1;
		$ipages[$okey]["menuname_". $root["page_lang"]] = $root["page_menuname"];
		$ipages[$okey]["subname_" . $root["page_lang"]] = $h2;
		$ipages[$okey]["title_" . $root["page_lang"]] = $root["page_title"];
		$ipages[$okey]["url"] = "/index.php" . ($root["page_slug"] !== "/"? "?slug=".$root["page_slug"]:"");							
		$ipages[$okey]["urlRW_" . $root["page_lang"]] = $root["page_slug"];
		if (!array_key_exists("cls", $ipages[$okey])){
			$ipages[$okey]["cls"] = $root["page_menuclass"];
		}else $ipages[$okey]["cls"] .= " " . $root["page_menuclass"];
		if (!array_key_exists("id", $ipages[$okey])){
			$ipages[$okey]["id"] = $root["page_lang"] . $root["page_id"];
		}else $ipages[$okey]["id"] .= $root["page_lang"] . $root["page_id"];
		$ipages[$okey]["lastmod_" . $root["page_lang"]] = $root["page_modified"];
		$ipages[$okey]["started_" . $root["page_lang"]] = $root["page_started"];
		$ipages[$okey]["ended_" . $root["page_lang"]] = $root["page_ended"];
		$ipages[$okey]["ended_" . $root["page_lang"]] = $root["page_ended"];
		$ipages[$okey]["description_" . $root["page_lang"]] = $root["page_description"];
		/*$ipages[$okey]["parent"] = $root["page_parent"];*/
		$ipages[$okey]["items"] = NULL;
		foreach($pages as $id => $page){
			if ($page["page_parent"] == $root["page_id"]){
				preprocessPages_pcpc($pages, $page, $ipages, $okey . '_' . $page["page_order"]);
			}
		}
	}
/**
	 * [fetchMenu description]
	 * @param  [type]  $sid [description]
	 * @param  boolean $raw [description]
	 * @return [type]       [description]
	 */
	function fetchMenu_with_description($sid = NULL, $raw = false){
		global $siteid, $db;
		$pages = array();
		$ipages = array();
		$menu = array();
		$roots = array();
		if ($sid == NULL) $sid = $siteid;
		$query = "SELECT page_id, page_parent, page_order, page_lang, page_slug, page_name, page_title,page_menuname, page_menuclass, page_modified, page_started, page_ended, page_description FROM cms_pages WHERE site_id = " . $sid . " AND page_in_menu = 1 AND `site_id` = " . $sid . " ORDER BY page_parent, page_order, page_lang";
		if (($result = mysql_query($query)) !== false) {
			
			while (($page = mysql_fetch_assoc($result)) !== false) {
				
				foreach ($page as $key => &$value){ 
					if (is_string($value)) $value = mb_convert_encoding($value, "UTF-8", "ISO-8859-1");
					//echo $key;
					//echo $value;
				}
				$pages[$page["page_id"]] = $page;
				if ($page["page_parent"] == "") array_push($roots, $page["page_id"]);
			}
		}
		
		foreach ($roots as $root){

			preprocessPages_pcpc($pages, $pages[$root], $ipages, '0');
		}
		//print_r($ipages);
		if ($raw) return $ipages;
		if (is_array($ipages) && array_key_exists('0', $ipages)){
			$menu = $ipages['0'];
			constructMenu($ipages, $menu["items"], '0');
		}
		//print_r($menu);
		return $menu;
	}
/**
	 * [menuToBootstrappedHTMLv2 returns a menu with submenu description]
	 * @param  [type]  $root     Beginning of menu structure
	 * @param  integer $maxl     Levels of menu to integrate -1 for all levels.
	 * @param  integer $lvl      Recursiveness level of father
	 * @param  boolean $open     Recursiveness of children
	 * @param  array   $exclude  Which children/grandchildren to exclude
	 * @param  [type]  $idprefix Prefix to be added to id's
	 * @param  boolean $nofollow Defines whether all links be no follow
	 * @return [type]            HTML code for menu.
	 */
	function menuToBootstrappedHTMLv2($menuArray, $maxl=-1, $l=0, $open=false, $exclude=array(), $idprefix=NULL, $nofollow=false){
		global $urlRW, $lang, $slug;
		$html = "";
		/* Exclusions */
		if (is_array($menuArray) && !in_array(getItemTitleName($menuArray, true), $exclude) && !in_array($menuArray["url"], $exclude) && !in_array($menuArray["urlRW_".$lang], $exclude) ){

			$n = $l + 1;
			
			/* Go in this if, if it is NOT the first level of recursiveness, so this will be called when defining the second level of the menu and so on */
			if ($l > 0){
				$cls = ($menuArray["cls"] == NULL? "":$menuArray["cls"]);

				if ($menuArray["url"] == NULL){
					$html .=  "<li data-temp='first".$n."'". ($cls !== ""? (' class="' . $cls . '"'):'')  . ">" . "\r\n";
					$html .= "\t" . "<h4>" . getItemTitleName($menuArray, true) . "</h4>" . "\r\n";	
				}else{
					
					$p = stripos($slug, pickLink($menuArray));
					$q = stripos(pickLink($menuArray), $slug);					
					if (!$open){
						if (($p !== FALSE && $p < 2) && ($q !== FALSE && $q < 2)) $cls .= " active ";

						$html .=  "<li data-temp='second".$n."'" . ($cls !== ""? (' class="' . $cls . '"'):'')  . ">" . "\r\n";
						$html .= "\t" . '<a data-temp="arriba"  ' . ($nofollow? 'rel="nofollow" ':'') . 'href="' . pickLink($menuArray) . '"' . ($menuArray["id"] !== NULL && $idprefix !== NULL? (' id="' . $idprefix . $menuArray["id"] . (is_numeric(str_replace($lang,'',$menuArray["id"]))? 'k':'') . '"'):'' ) . '>' . getItemTitleName($menuArray, true) . "</a>" . "\r\n";	
					}else{
						if (($p !== FALSE && $p < 2) && ($q !== FALSE && $q < 2)){ 
							$cls = "active ";
						}else $cls = "";
						if($n===2){
							$cls.="dropdown";
							$lnkCls='dropdown-toggle';
						}
						if ($menuArray["items"] !== NULL && ($maxl==-1 || (!$open && $n < $maxl) || ($open && $n <= $maxl))){

						}
						$html .=  "<li data-temp='third".$n."'" . ($cls !== ""? (' class="' . $cls . '"'):'') . ">" . "\r\n";
						$showLink = true; //($maxl==-1 || $n > $maxl);
						$html .= "\t" . '<a data-temp="abajo"' . ($nofollow == true? 'rel="nofollow" ':'') . 'href="' . (($menuArray["items"] == NULL || $showLink)? pickLink($menuArray):"#") . '"' . ($lnkCls !== ""? (' class="' . $lnkCls . '" '):'') . '>' . getItemTitleName($menuArray, true) . "</a>" . "\r\n"; //."|".$n."|".$maxl // title="' . $open . ',' . $maxl . '==-1||' . $n . '>' . $maxl . '"
					}
				}
			}
			if ($menuArray["items"] !== NULL && ($maxl==-1 || (!$open && $n < $maxl) || ($open && $n <= $maxl))){	
				
				if ($l || !$open) $html .= "<div class='row'><div class='submenu_description col-lg-4'><p class='title'>aqui va la descripcion</p></div>\t<div class='col-lg-8'><ul" . ($menuArray["id"] !== NULL && $idprefix !== NULL? (' id="' . $idprefix . $menuArray["id"] . '"'):'' ) . " class='dropdown-menu'>\r\n";
				
				foreach ($menuArray["items"] as $item){
					$html .= menuToBootstrappedHTMLv2($item, $maxl, $l+1, $open, $exclude, $idprefix, $nofollow);
				}
				if ($l || !$open) $html .= "\t</ul></div></div>";
				
			}
			if ($l > 0) $html .=  "</li>" . "\r\n";
		}
		return $html;
	}

/**
	 * [menuToHTML description]
	 * @param  [type]  $array       Array containing elements of menu from DB
	 * @param  integer $levels      Levels to display 1 to 4
	 * @param  string  $father      Father from which form menu in HTML
	 * @param  boolean $allLevels   If set true, brings all levels from father
	 * @return string  $html        HTML code for menu
	 */

	function menuToHTMLPCPC($array, $levels, $father, $allLevels=false){
		global $urlRW, $lang, $slug, $counterLevels;
		$name_lang="name_".$lang;
		//echo $name_lang;
		$html = '<ul>';

		if (is_array($array)){

			foreach ($array as $key => $value) {

				foreach($value as $newArray){	
			 		 if(($levels == 1 && $newArray[$name_lang] == $father) || 
			 			($newArray[$name_lang] == $father && $allLevels)){
			 			$html .= '<li class="level1" id="'.$newArray['id'].'"><a href="'.$newArray['urlRW_en'].'">'.$newArray[$name_lang].'</a></li>';	
			 		}
			 		foreach ($newArray['items'] as $value2) {	
			 			//echo $value2[$name_lang];
			 			if(($levels == 2 && $newArray['name_en'] == $father) || 
			 				($newArray['$name_lang'] == $father && $allLevels)){
				 			$html .= '<li class="level2"><a href="'.$value2['urlRW_'.$lang].'">'.$value2[$name_lang].'</a></li>';		
				 		}						
		 				foreach ($value2['items'] as $value3) {						 					
		 					if(($levels == 3 && $newArray['$name_lang'] == $father) || 
		 						($newArray['$name_lang'] == $father && $allLevels)){
					 			$html .= '<li class="level3"><a href="'.$value3['urlRW_'.$lang].'">'.$value3['$name_lang'].'</a></li>';
					 		}											
			 				foreach ($value3['items'] as $value4) {
			 					if(($levels == 4 && $newArray['$name_lang'] == $father) || 
			 						($newArray['$name_lang'] == $father && $allLevels)){
						 			$html .= '<li class="level4"><a href="'.$value4['urlRW_'.$lang].'">'.$value4['$name_lang'].'</a></li>';
						 		}
			 				}								 				

		 				}
			 		}											
				}																
			}
		}
		
		return $html.'</ul>';
	}
?>
