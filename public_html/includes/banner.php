<div class="banner">
	<span id='closeglobal' class="close">x</span>
	<div class="left_part"> 
		<span id='closemobile'>x</span>
		<div class="booknow">Book now!</div>
		<div class="reserve">Reserve your Stay at Meliá Hotel</div>
	</div>
	<div class="central_part"> 
		<div class="checkin_container">
			<div class="checkin"> 
				Check-in
				<span class="checkin_initial">26</span>
		    	<div class='input-group date checkin_initial_date' id='datetimepicker1'>
			        <input type='text' class="form-control" />
			        <span class="input-group-addon">
			        	<span class="glyphicon glyphicon-calendar " style="font-size:1.2em;"></span>
			        </span>
		    	</div>
			</div>
			<div class="divider">
				<img class="banner_right" src="/images/pcpc_banner_separator.png" alt="Divider">	
			</div>
			<div class="checkout"> 
				Checkout
				<span class="checkout_initial">03</span>
			    <div class='input-group date checkout_initial_date' id='datetimepicker2'>
			        <input type='text' class="form-control" />
			        <span class="input-group-addon">
			        	<span class="glyphicon glyphicon-calendar " style="font-size:1.2em;"></span>
			        </span>
			    </div>
			</div>
		</div>
	</div>	
	<div class="right_part ">
		<span id='close' >x</span>
	 	<span class="availability">Click Here to Apply</span> 
		<span class="availability_button">
			<button type="button" class="btn btn-primary">BOOK AVAILABILITY ></button>
		</span>
	</div>