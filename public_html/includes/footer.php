	<footer id="footer">
		<!-- <a href="https://www.americascardroom.eu/blog/"><img src="/images/pcpc_event.jpg" class="img-responsive"></a> -->
		<!-- <div class="countdown_container">

			<div class="countdown_title">
				<img src="/images/pcpc_countdown.png" alt="Punta Cana Countdown"><span class="countdown_bold"><?php echo ($lang=="en" ? "Countdown":"Cuenta Regresiva"); ?></span><img src="/images/pcpc_countdown_separator.png" alt="Divider"><span class="countdown_light"><?php echo ($lang=="en" ? "We’re close to the next Punta Cana:":"Se acerca el pr&oacute;ximo Punta Cana:"); ?></span>
			</div>
			<div class="countdown_counter">
				<ul class="countdown">
			      <li>
			        <span class="days"></span>
			        <p class="timeRefDays"></p>
			      </li>
			      <li>
			        <span class="hours"></span>
			        <p class="timeRefHours"></p>
			      </li>
			      <li>
			        <span class="minutes"></span>
			        <p class="timeRefMinutes"></p>
			      </li>
			      <li>
			        <span class="seconds"></span>
			        <p class="timeRefSeconds"></p>
			      </li>
				</ul>
			</div>
		</div> -->
		<div class="footer_container">
			<div class="footer_box_left col-md-6 col-sm-12">
				<span class="footer_bold">© 2016 </span><span class="footer_bold_highlight">Punta Cana Poker Classic.</span><span class="footer_bold"> <?php echo ($lang=="en" ? "All Rights Reserved":"Todos los Derechos Reservados"); ?></span>
				<span class="footer_medium"><?php echo ($lang=="en" ? "Punta Cana and related trademarks are registered trademarks of WPN Poker Network":"Punta Cana y marcas relacionadas son marcas registradas de WPN Poker Network"); ?></span>
				<span class="footer_medium_highlight"><a href="<?php echo ($lang=="en" ? "/tournament/cancellation-policies/":"/torneo/politicas-cancelacion/"); ?>"> <?php echo ($lang=="en" ? "Privacy Policy":"Pol&iacute;tica de Privacidad"); ?> | <?php echo ($lang=="en" ? "Terms and Conditions":"T&eacute;rminos y Condiciones"); ?> | <?php echo ($lang=="en" ? "Responsible Gambling":"Juego Responsable"); ?></a></span> 
			</div>

			<div class="footer_box_right col-md-6 col-sm-12">
				<span class="footer_bold contact_us"><?php echo ($lang=="en" ? "Contact Us":"Cont&aacute;ctenos"); ?></span>
				<div class="social_icons">
					<a href="https://www.facebook.com/PuntaCanaPokerClassic" target="_blank"><img src="/images/pcpc_facebook_icon.jpg" alt="Punta Cana Poker Classic Facebook"></a>
					<a href="https://twitter.com/PuntaCanaPoker" target="_blank"><img src="/images/pcpc_twitter_icon.jpg" alt="Punta Cana Poker Classic Twitter"></a>
					<a href="https://www.youtube.com/user/PuntaCanaPoker" target="_blank"><img src="/images/pcpc_youtube.png" alt="Punta Cana Poker Classic Youtube"></a>
				</div>
				<span class="footer_medium">Punta Cana Poker Classic</span>
				<span class="footer_medium">Dominican Republic Live Poker Event</span>
				<span class="footer_medium"><?php echo ($lang=="en" ? "Phone:":"Tel&eacute;fono:"); ?>  1-800-527-4048</span>
				<span class="footer_medium">Email: Info@PuntaCanaPokerClassic.com</span>
				<div class="footer_logos">
					<img src="/images/pcpc_logos_footer.png" class="img-responsive" alt="Punta Cana Poker Classic Melia Logos">
				</div>
			</div>
		</div>
	</footer>

	<script src="<?php echo timestampUrl('/js/jquery-1.12.3.min.js'); ?>"></script>
	<script src="<?php echo timestampUrl('/js/moment.js'); ?>"></script>
	<script src="<?php echo timestampUrl('/js/transition.js'); ?>"></script>
	<script src="<?php echo timestampUrl('/js/collapse.js'); ?>"></script>
	<script src="<?php echo timestampUrl('/js/bootstrap.min.js'); ?>"></script>
	<script src="<?php echo timestampUrl('/js/bootstrap-validator.min.js'); ?>"></script>
	<script src="<?php echo timestampUrl('/js/bootstrap-datetimepicker.min.js'); ?>"></script>
	<script src="<?php echo timestampUrl('/js/bootstrap-formhelpers.min.js'); ?>"></script>		
	
	<?php
	    if ($mainCls == "homepage") { ?>
	      	<!-- REVOLUTION JS FILES -->
	      	<script src="<?php echo timestampUrl('/js/revolution/jquery.themepunch.tools.min.js'); ?>"></script>
	      	<script src="<?php echo timestampUrl('/js/revolution/jquery.themepunch.revolution.min.js'); ?>"></script>
			<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->	
			<script src="<?php echo timestampUrl('/js/revolution/extensions/revolution.extension.layeranimation.min.js'); ?>"></script>
			<script src="<?php echo timestampUrl('/js/revolution/extensions/revolution.extension.navigation.min.js'); ?>"></script>
			<script src="<?php echo timestampUrl('/js/revolution/extensions/revolution.extension.parallax.min.js'); ?>"></script>
			<script src="<?php echo timestampUrl('/js/revolution/extensions/revolution.extension.slideanims.min.js'); ?>"></script>
			<script src="<?php echo timestampUrl('/js/revolution/extensions/revolution.extension.video.min.js'); ?>"></script>
	<?php } else if ($mainCls == "pcpc_media") { ?>
			<script src="<?php echo timestampUrl('/js/lightbox.js'); ?>"></script>
	<?php } else if ($mainCls == "payout") { ?>	    
	    	<script type="text/javascript" src="/js/footable.all.min.js"></script>
	<?php } ?> 
	
	<?php if ($lang == "en") { ?>		
			// <!-- <script src="<?php //echo timestampUrl('/js/counter_en.js'); ?>" type="text/javascript"></script> -->
	<?php }else{ ?>		
			<!-- <script src="<?php //echo timestampUrl('/js/counter_es.js'); ?>" type="text/javascript"></script> -->
	<?php } ?>

	<script src="<?php echo timestampUrl('/js/gsap/minified/TweenMax.min.js'); ?>"></script>
	<script src="<?php echo timestampUrl('/js/gsap/minified/plugins/MorphSVGPlugin.min.js?r=182'); ?>"></script>
	<script src="<?php echo timestampUrl('/js/gsap/minified/plugins/DrawSVGPlugin.min.js'); ?>"></script>	
	<script src="<?php echo timestampUrl('/js/jquery.validate.min.js'); ?>"></script>	
	<script src="<?php echo timestampUrl('/js/pcpc' . ($dbtype!=='test'? '.min':'') . '.js'); ?>"></script>	
	
	<!-- START OF ANALYTICS CODE -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-36534718-13', 'auto');
		ga('send', 'pageview');
	</script>
	<!-- END OF ANALYTICS CODE --> 

</body>    
</html>