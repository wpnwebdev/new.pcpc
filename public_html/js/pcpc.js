/*------------------------------------*\
#VARIABLES
\*------------------------------------*/

function getInstagramFeed(){
	jQuery.ajax({
		url: "/includes/fetchInstagramFeed.php?r=" + Math.floor(Math.random()*10001),
		type: "get",
		dataType: 'json',
		timeout: 15000,
		success: function(data, textStatus, jqXHR){
			//console.log(data);
			if (typeof data == "object" && data !== false){
				$.each( data.data, function( key, value ) {
					var urlLink = data.data[key].link;
					var urlImage = data.data[key].images.thumbnail.url;

					if(urlImage != undefined){
						$('<img>').attr('src',urlImage).appendTo('.instagramFeed').wrap('<a href='+ data.data[key].link +' target=_blank>');
					}					
				});
			}
		}
	});
}
/*------------------------------------*\
#GALLERIES
\*------------------------------------*/

// function get_gallery(folder){

// 	var thumbnails = folder + "thumbs/";
// 	var web = folder + "web/"

// 	$( "#gallery" ).empty();
// 	$.ajax({
// 	    url : thumbnails,
// 	    success: function (data) {
// 	        $(data).find("a").attr("href", function (i, val) {
// 	            if( val.match(/\.(jpe?g|png|gif)$/) ) { 
// 	                $("#gallery").append( "<a href='"+web+val+"' data-lightbox='roadtrip' ><img src='"+thumbnails+val+"'></a>" );
// 	            } 
// 	        });
// 	    }
// 	});

// }

function get_gallery(folder){

	$( "#gallery" ).empty();
	$.ajax({
		url: '/includes/galleries.php?show='+folder,
	    success: function (data) {
	        $("#gallery").append( data);
	    }
	});

}

$(document).ready(function(){
	//The instagram feed will be back in October 2017
	// getInstagramFeed();

	if ( (!$('body').hasClass('booknow')) && (!$('body').hasClass('homepage'))){
		// document.getElementById('close').onclick = function(){
	 //        this.parentNode.parentNode.parentNode
	 //        .removeChild(this.parentNode.parentNode);
	 //        return false;
  //   	};

  //   	document.getElementById('closemobile').onclick = function(){
	 //        this.parentNode.parentNode.parentNode
	 //        .removeChild(this.parentNode.parentNode);
	 //        return false;
  //   	};
   
  //   	document.getElementById('closeglobal').onclick = function(){
	 //        this.parentNode.parentNode.parentNode
	 //        .removeChild(this.parentNode.parentNode);
	 //        return false;
  //   	};	
	}

	$("#datetimepicker1").datetimepicker({
 	 	format: "MM/DD/YYYY",
        defaultDate: moment().subtract(0, 'days'),
        useCurrent: true
 	});

 	$("#datetimepicker2").datetimepicker({
 	 	format: "MM/DD/YYYY",
        defaultDate: moment().subtract(0, 'days'),
        useCurrent: true
 	});

	if($(window).width() < 767){
		$('a#menuh-item-en55036es55037').on('click', function(event) {
			event.preventDefault(); 
			event.stopPropagation(); 
			$("ul#menuh-item-en55036es55037").toggleClass('open');
			$(this).parent().siblings().removeClass('open');
			$(this).parent().toggleClass('open');
		});
	}

	$('#BookingForm').validate({
    
        // Specify the validation rules
        rules: {
            name: "required",
            last_name: "required",
            email: {
                required: true,
                email: true
            },
            address: "required",
            phone: "required"
           
        },
        // Specify the validation error messages
        messages: {
            name: "Please enter your first name",
            last_name: "Please enter your last name",
            email: "Please enter a valid email address",
            address: "Please enter a valid address",
            phone: "Please enter a valid phone number"
        },
        
        submitHandler: function(form) {
            form.submit();
        }
    });
});

var	menuSelection = 0,
dropdownMenu = $('#dropdown-menu-head').clone();
//Show Menu when hover on menu header items
if($(window).width() >= 767){
	$("header>.mainmenu>.nav-justified>li>a").hover( function () {
		menuSelection = $(this); 		
		if(menuSelection.attr('id') !==  null){		
			switch(menuSelection.attr('id')){
				case 'menuh-item-en55036es55037':
					$('#dropdown-menu-head').stop(true, true).fadeIn();
					break;
	  			default:
	  				$('#dropdown-menu-head').css({'display':'none'});
	  				break;
	  		}
	  	}
	});
}

$("header>.dropdown-menu").mouseleave( function () {
  	$('#dropdown-menu-head').css({'display':'none'});  	
});
	
if ($("body").hasClass("payout")){
	$('.footable').footable( {
		breakpoints: {
		phone: 450,
	    medium: 555,
	    tablet: 768,
	    tablethorizontal: 950,
	    big: 2048
		}

	});
}	


/*------------------------------------*\
#TABS for the inner page template
\*------------------------------------*/
if ($("body").hasClass("pcpc_media")){

	get_gallery("FinalTable")

}


/*------------------------------------*\
#TABS for the inner page template
\*------------------------------------*/
if ($("body").hasClass("tabs")){
	var tab = $(this).children().attr('href');
	$('.nav-pills a[href="#tabs-1"]').tab('show')
	$(".tab-content #tabs-1").addClass("in active");
}

/*------------------------------------*\
#Submenu for the tournaments template
\*------------------------------------*/

$('#tournament-menu > ul.menu-buttons li').click(function(e) {
    $('.options li.active').removeClass('active');
    var $this = $(this);
    $this.addClass('active');
    e.preventDefault();
});


if ($("body").hasClass("booknow")){
	$("#datetimepicker1bookings").datetimepicker({
	 	format: "MM/DD/YYYY",
        defaultDate: moment().subtract(0, 'days'),
        useCurrent: true
	});

	$("#datetimepicker2bookings").datetimepicker({
		format: "MM/DD/YYYY",
        defaultDate: moment().subtract(0, 'days'),
        useCurrent: true
	});
}

if ($("body").hasClass("pcpc_media")){
	// $('#video-gallery').lightGallery({
	// 	loadYoutubeThumbnail: true,
 //    	youtubeThumbSize: 'default'

	// }); 
	// $('#responsive-images').lightGallery({
	// 	counter: false,
 //    	download: false,
 //    	thumbnail:true,
 //    animateThumb: false,
 //    showThumbByDefault: false
	// 	}); 
}
if ($("body").hasClass("homepage")){

	var tpj=jQuery;					
	var revapi112;
	tpj(document).ready(function() {
		if(tpj("#rev_slider_112_1").revolution == undefined){
			revslider_showDoubleJqueryError("#rev_slider_112_1");
		}else{
			revapi112 = tpj("#rev_slider_112_1").show().revolution({
				sliderType:"Flat-Infinite",
				jsFileLocation:"/revolution/js/",
				sliderLayout:"fullwidth",
				dottedOverlay:"none",
				delay:40000,
				navigation: {
					keyboardNavigation:"off",
					keyboard_direction: "horizontal",
					mouseScrollNavigation:"off",
					onHoverStop:"off",
					touch:{
						touchenabled:"on",
						swipe_threshold: 75,
						swipe_min_touches: 1,
						swipe_direction: "horizontal",
						drag_block_vertical: false
					}
					,
					bullets: {
						enable:true,
						hide_onmobile:true,
						hide_under:600,
						style:"hermes",
						hide_onleave:true,
						hide_delay:200,
						hide_delay_mobile:1200,
						direction:"horizontal",
						h_align:"right",
						v_align:"top",
						h_offset:30,
						v_offset:15,
						space:5,
						tmp:''
					}
				},
				viewPort: {
					enable:true,
					outof:"pause",
					visible_area:"100%"
				},
				responsiveLevels:[1440,1240,1024,610,530],
				gridwidth:[1920,1440,1240,1044,610,530],
				gridheight:[720,720,720,700,700,600],
				lazyType:"none",
				parallax: {
					type:"mouse",
					origo:"slidercenter",
					speed:2000,
					levels:[2,3,4,5,6,7,12,16,10,50],
				},
				shadow:0,
				spinner:"off",
				stopLoop:"off",
				stopAfterLoops:-1,
				stopAtSlide:-1,
				shuffle:"off",
				autoHeight:"off",
				hideThumbsOnMobile:"off",
				hideSliderAtLimit:0,
				hideCaptionAtLimit:0,
				hideAllCaptionAtLilmit:0,
				debugMode:false,
				fallbacks: {
					simplifyAll:"off",
					nextSlideOnWindowFocus:"off",
					disableFocusListener:false,
				}
			});
		}
	});	/*ready*/
}

// function SplashBeGone() { 
//   document.getElementById('splash').style.display = 'none'; 
// } 
// function Init() { 
//   document.getElementById('splash').style.display = 'block'; 
//   setTimeout(function(){ SplashBeGone(); }, 5000); 
// } 
// window.onload = Init;

TweenMax.to(".sun",2,{scale:4.5,x:-66.5,y:84.5,ease:Power4.easeIn});
TweenMax.to("#ocean",2,{scale:1.25,x:-30,y:-61.5,ease:Power4.easeIn});


var tl = new TimelineMax({repeat:-1, repeatDelay:3, delay:4});


tl.to("#rightcenter",3,{scale:1.2,rotation:-3,yoyo:true,transformOrigin:"left bottom",ease: Elastic.easeOut.config(2.5, 0.5)},'2.0');
tl.to("#rightleaf1",3,{rotation:-3,yoyo:true,transformOrigin:"left bottom",ease: Elastic.easeOut.config(2.5, 0.5)},'2.0');
tl.to("#rightleaf2",10,{rotation:-3,yoyo:true,transformOrigin:"left bottom",ease: Elastic.easeOut.config(2.5, 0.5)},'2.0');
tl.to("#rightleaf3",10,{rotation:-3,yoyo:true,transformOrigin:"left bottom",ease: Elastic.easeOut.config(2.5, 0.5)},'2.0');
tl.to("#rightleaf4",10,{rotation:-3,yoyo:true,transformOrigin:"left bottom",ease: Elastic.easeOut.config(2.5, 0.5)},'2.0');
tl.to("#rightleaf5",10,{rotation:-8,yoyo:true,transformOrigin:"left bottom",ease: Elastic.easeOut.config(2.5, 0.5)},'2.0');
tl.to("#rightleaf6",10,{rotation:-3,yoyo:true,transformOrigin:"left bottom",ease: Elastic.easeOut.config(2.5, 0.5)},'2.0');



tl.to("#leftcenter",3,{scale:1.2,rotation:-3,yoyo:true,transformOrigin:"left bottom",ease: Elastic.easeOut.config(2.5, 0.5)},'2.0');
tl.to("#leftleaf1",3,{rotation:-3,yoyo:true,transformOrigin:"left bottom",ease: Elastic.easeOut.config(2.5, 0.5)},'2.0');
tl.to("#leftleaf2",10,{rotation:-3,yoyo:true,transformOrigin:"left bottom",ease: Elastic.easeOut.config(2.5, 0.5)},'2.0');
tl.to("#leftleaf3",10,{rotation:-3,yoyo:true,transformOrigin:"left bottom",ease: Elastic.easeOut.config(2.5, 0.5)},'2.0');
tl.to("#leftleaf4",10,{rotation:-3,yoyo:true,transformOrigin:"left bottom",ease: Elastic.easeOut.config(2.5, 0.5)},'2.0');
tl.to("#leftleaf5",10,{rotation:-8,yoyo:true,transformOrigin:"left bottom",ease: Elastic.easeOut.config(2.5, 0.5)},'2.0');
tl.to("#leftleaf6",10,{rotation:-3,yoyo:true,transformOrigin:"left bottom",ease: Elastic.easeOut.config(2.5, 0.5)},'2.0');

tl.to("#animated_x5F_waves", 2, {x:-341.5,y:-105.5,scale:7.5},'-2')
tl.to("#small_x5F_waves",1,{rotation:32,transformOrigin:"left bottom",scale:5,x:-100,y:-65,ease:Power1.easeInOut},'-1');

tl.to("#puntacana",0.2,{scale:1.05,yoyo:true,ease:Power1.easeInOut});
tl.to("#puntacana",0.2,{scale:1,yoyo:true,ease:Power1.easeInOut});
tl.to("#puntacana",0.2,{scale:1.05,yoyo:true,ease:Power1.easeInOut});
tl.to("#puntacana",0.2,{scale:1,yoyo:true,ease:Power1.easeInOut});

tl.restart();
