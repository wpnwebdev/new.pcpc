<?php
if (isset($page)){
	require_once("includes/config.php");
	$ts = explode("|", $page["page_title"]);
	$t1 = $ts[0]; $t2 = ((count($ts) > 1)? $ts[1]:'');
	$title = (isNotEmpty($page["page_title"])? $page["page_title"]:$defTitle);
	$stitle = $t1;
	$description = (isNotEmpty($page["page_description"])? $page["page_description"]:(isNotEmpty($page["page_content"])? getDescriptionFromContent($page["page_content"]):$defDescription));
	$keywords = (isNotEmpty($page["page_keywords"])? $page["page_keywords"]:(isNotEmpty($page["page_content"])? getKeywordsFromContent ($page["page_content"]):$defDescription));
	$created = (isset($page['page_created']) && $page['page_created'] !== "0000-00-00 00:00:00"? $page['page_created']:(isset($page['page_modified']) && $page['page_modified'] !== "0000-00-00 00:00:00"? $page['page_modified']:date('Y-m-dTH:i:s'))); $created = str_replace(' ', 'T', $created);
	$modified = (isset($page['page_modified']) && $page['page_modified'] !== "0000-00-00 00:00:00"? $page['page_modified']:(isset($page['page_created']) && $page['page_created'] !== "0000-00-00 00:00:00"? $page['page_created']:date('Y-m-d H:i:s'))); $modified = str_replace(' ', 'T', $modified);
	$mainCls = $page["page_style"];
	require_once("includes/header.php");
	$checkin=$_POST['checkin_date'];
	$checkout=$_POST['checkout_date'];
	if ( (trim($checkin) !="") && (trim($checkout) !="") ){
		$checkin_data=explode("/",$checkin);
		$checkout_data=explode("/",$checkout);
	}else{
		$checkin= date("m/d/Y");  
		$checkout=date("m/d/Y");  
		$checkin_data=explode("/",$checkin);
		$checkout_data=explode("/",$checkout);
	}
?>
<div class="container-fluid">
	<div class="page_title"><?php echo $title;?></div>
	<div class="page_title_border"></div>
	<?php echo $page["page_head"];?>
	<div class="page_content no_padding full-width">
		<div class="page_content "><?php echo html_entity_decode(captchaReplace($page["page_content"])); ?></div><!-- content container ends here -->
		<!-- FORM -->
		<div class="row nomargins">
			<form action="/confirmation/" class="form-horizontal form_size" id="BookingForm" method="post" role="form">
				<div class="booking-form ">
					<div class="col-md-6 col-sm-12">
						<div class="form-group">
							<label class="col-sm-3 control-label" for="Name"><?php  echo ($lang=="en" ? "Name:":"Nombre:"); ?></label>
							<div class="col-sm-9">
								<input class="form-control input-lg" id="Name" name="name" placeholder=<?php  echo ($lang=="en" ? "Your Name":"'Su Nombre'"); ?> type="text" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="first_name"><?php  echo ($lang=="en" ? "Last Name:":"Apellido:"); ?></label>
							<div class="col-sm-9">
								<input class="form-control input-lg" id="first_name" name="last_name" placeholder=<?php  echo ($lang=="en" ? "'Last Name'":"Apellido"); ?> type="text" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="Country"><?php  echo ($lang=="en" ? "Country:":"País:"); ?></label>
							<div class="col-sm-9">
								<select class="input-medium bfh-countries countries input-lg" data-country="US" name="country" style="width: 100%;border-color: lightgray;color: #999;"></select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="Email"><?php  echo ($lang=="en" ? "Email:":"Email:"); ?></label>
							<div class="col-sm-9">
								<input class="form-control input-lg" id="Email" name="email" placeholder="Email" type="email" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="Adress"><?php  echo ($lang=="en" ? "Address:":"Dirección:"); ?></label>
							<div class="col-sm-9">
								<input class="form-control input-lg" id="Address" name="address" placeholder=<?php  echo ($lang=="en" ? "'Your Address'":"Dirección"); ?> type="text" />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label" for="Phone"><?php  echo ($lang=="en" ? "Phone:":"Teléfono:"); ?></label>
							<div class="col-sm-9">
								<input class="form-control input-lg" id="Phone" name="phone" placeholder=<?php  echo ($lang=="en" ? "'Phone Number'":"Phone"); ?> type="text" />
							</div>
						</div>
					</div>	
					<div class="booking-calendar col-md-6  col-sm-12 ">
						<div class="booking_container">
							<div class="checkin_booking">
								<?php  echo ($lang=="en" ? "Check-in:":"Ingreso:"); ?> <span class="checkin_initial_booking"><?php echo $checkin_data[1]; ?></span>
								<div class="input-group date checkin_initial_date_booking" id="datetimepicker1bookings">
									<input class="form-control" name="checkin" type="text" value=<?php echo $checkin; ?> /> <span class="calendar_bookings_format input-group-addon "><span class="glyphicon glyphicon-calendar " style="font-size:1.3em;"> </span></span>
								</div>
							</div>
							<div class="checkout_booking">
								<?php  echo ($lang=="en" ? "Checkout:":"Salida:"); ?> <span class="checkout_initial_booking"><?php echo $checkout_data[1]; ?></span>
								<div class="input-group date checkout_initial_date_booking" id="datetimepicker2bookings">
									<input class="form-control" name="checkout" type="text" value=<?php echo $checkout; ?> /> <span class="calendar_bookings_format input-group-addon "><span class="glyphicon glyphicon-calendar " style="font-size:1.3em;"> </span></span>
								</div>
							</div>
						</div>
						<div class="form-group inline-block">
							<label class="col-sm-3 control-label" for="Rooms"><?php  echo ($lang=="en" ? "Rooms:":"Habitaciones:"); ?></label>
							<div class="col-sm-9">
								<select class="form-control form_select" id="Rooms" name="rooms"><option selected="selected">01</option><option>02</option><option>03</option><option>04</option><option>05</option><option>06</option><option>07</option><option>08</option><option>09</option><option>10</option><option>11</option><option>12</option></select>
							</div>
						</div>
						<div class="form-group inline-block">
							<label class="col-sm-3 control-label" for="Adults"><?php  echo ($lang=="en" ? "Adults:":"Adultos:"); ?></label>
							<div class="col-sm-9">
								<select class="form-control form_select" id="Adults" name="adults"><option>01</option><option selected="selected">02</option><option>03</option><option>04</option><option>05</option><option>06</option><option>07</option><option>08</option><option>09</option><option>10</option><option>11</option><option>12</option></select>
							</div>
						</div>
						<div class="form-group inline-block">
							<label class="col-sm-3 control-label" for="Children"><?php  echo ($lang=="en" ? "Children:":"Niños:"); ?></label>
							<div class="col-sm-9">
								<select class="form-control form_select" id="Children" name="children"><option selected="selected">00</option><option>01</option><option>02</option><option>03</option><option>04</option><option>05</option><option>06</option><option>07</option><option>08</option><option>09</option><option>10</option><option>11</option><option>12</option></select>
							</div>
						</div>
						<div class="form-group inline-block">
							<label class="col-sm-3 control-label" for="Comments"><?php  echo ($lang=="en" ? "Comments:":"Comentarios:"); ?></label>
							<div class="col-sm-9">
								<textarea class="form-control" id="Comments" name="comments" rows="5"></textarea>
							</div>
						</div>
					</div>
					<!-- row ends here --><!-- Calendario -->			
					<div class="row nomargins">
						<div class="form-group center col-lg-12 col-md-12 col-sm-12">
							<button class="btn btn-primary btn-lg booking_submit_button" type="submit"><?php  echo ($lang=="en" ? "SUBMIT":"ENVIAR"); ?> <img alt="submit" src="/images/pcpc_submit_arrow.png" /></button>
						</div>
					</div>
				</div><!-- row ends here --><!-- form ends here -->		
			</form><!-- FORM -->
		<div><!-- container ends here -->	
	</div><!-- content ends here -->
</div>
<?php
		require_once("includes/footer.php");
	}
?>
