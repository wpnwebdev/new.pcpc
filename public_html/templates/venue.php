<?php
if (isset($page)){
	require_once("includes/config.php");
	$ts = explode("|", $page["page_title"]);
	$t1 = $ts[0]; $t2 = ((count($ts) > 1)? $ts[1]:'');
	$title = (isNotEmpty($page["page_title"])? $page["page_title"]:$defTitle);
	$stitle = $t1;
	$description = (isNotEmpty($page["page_description"])? $page["page_description"]:(isNotEmpty($page["page_content"])? getDescriptionFromContent($page["page_content"]):$defDescription));
	$keywords = (isNotEmpty($page["page_keywords"])? $page["page_keywords"]:(isNotEmpty($page["page_content"])? getKeywordsFromContent ($page["page_content"]):$defDescription));
	$created = (isset($page['page_created']) && $page['page_created'] !== "0000-00-00 00:00:00"? $page['page_created']:(isset($page['page_modified']) && $page['page_modified'] !== "0000-00-00 00:00:00"? $page['page_modified']:date('Y-m-dTH:i:s'))); $created = str_replace(' ', 'T', $created);
	$modified = (isset($page['page_modified']) && $page['page_modified'] !== "0000-00-00 00:00:00"? $page['page_modified']:(isset($page['page_created']) && $page['page_created'] !== "0000-00-00 00:00:00"? $page['page_created']:date('Y-m-d H:i:s'))); $modified = str_replace(' ', 'T', $modified);
	$mainCls = $page["page_style"];
	require_once("includes/header.php");
?>
<div class="container-fluid">

	<div class="page_title"><?php echo $title;?></div>
	<div class="page_title_border"></div>
	<?php echo $page["page_head"];?>

	<div class="page_content no_padding full-width">
		<?php echo html_entity_decode(captchaReplace($page["page_content"])); ?>		
		<?php if ($mainCls != "booknow") {	?>

	<!-- <form action="/book-now/" method="post"> -->
		<!-- <div class="banner_live"><a href="https://www.americascardroom.eu/blog/"><img src="/images/pcpc_event.jpg" class="img-responsive"></a> -->
			<!-- <span id='closeglobal' class="close">x</span>
			<div class="left_part"> <!-- Banner left -->
				<!-- <span id='closemobile'>x</span> -->
				<!-- <div class="booknow"><?php  // echo ($lang=="en" ? "Book now!":"Reserve ya!"); ?></div> -->
				<!-- <div class="reserve"><?php //echo ($lang=="en" ? "Reserve your Stay at Meliá Hotel":"Reserve su estad&iacute;a en el Hotel Meli&aacute;"); ?></div> -->
			<!-- </div> End of Banner left --> 
				<!-- <div class="central_part">  <!-- Banner center --> 
					<!-- <div class="checkin_container"> -->
						<!-- <div class="checkin"> 
							<?php //echo ($lang=="en" ? "Check-in":"Ingreso"); ?>
							<span class="checkin_initial">25</span>							
					    	<div class='input-group date checkin_initial_date' id='datetimepicker1'>
					        	<input type='text' class="form-control" NAME="checkin_date" value="10/25/2016" />
					     		<span class="input-group-addon">
					        		<span class="glyphicon glyphicon-calendar " style="font-size:1.2em;"></span>
						        </span>
					    	</div>
						</div> -->
						<!-- <div class="divider"> -->
							<!-- <img class="banner_right" src="/images/pcpc_banner_separator.png" alt="Divider">	 -->
						<!-- </div> -->
						<!-- <div class="checkout">  -->
							<?php //echo ($lang=="en" ? "Checkout":"Salida"); ?>
							<!-- <span class="checkout_initial">31</span>
						    <div class='input-group date checkout_initial_date' id='datetimepicker2'>
						        <input type='text' class="form-control" NAME="checkout_date" value="10/31/2016" />
						        <span class="input-group-addon">
						        	<span class="glyphicon glyphicon-calendar " style="font-size:1.2em;"></span>
						        </span>
						    </div> -->
			<!-- 			</div>
					</div>
			 <!--	</div> End of Banner center		 -->
				<!-- <div class="right_part "> <!-- Banner right --> 
					<!-- <span id='close' >x</span> -->
				 	<!-- <span class="availability"><?php //echo ($lang=="en" ? "Click Here to Apply":"Click para Aplicar"); ?></span>  -->
					<!-- <span class="availability_button"> -->
						<!-- <button type="submit" class="btn btn-primary"><?php //echo ($lang=="en" ? "BOOK AVAILABILITY >" : "DISPONIBILIDAD >"); ?> </button> -->
					<!-- </span>				 -->
				<!-- </div> End of Banner right -->
		</div> <!-- Banner ends here -->
	<!-- </form>	 -->
	<?php } ?>
	</div><!-- content ends here -->
</div>
<?php
		require_once("includes/footer.php");
	}
?>
