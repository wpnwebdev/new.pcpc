<?php
	if (isset($page)){
		require_once("includes/config.php");
		$ts = explode("|", $page["page_title"]);
		$t1 = $ts[0]; $t2 = ((count($ts) > 1)? $ts[1]:'');
		$title = (isNotEmpty($page["page_title"])? $page["page_title"]:$defTitle);
		$stitle = $t1;
		//$description = (isNotEmpty($page["page_description"])? $page["page_description"]:(isNotEmpty($page["page_content"])? getDescriptionFromContent($page["page_content"]):$defDescription));
		//$keywords = (isNotEmpty($page["page_keywords"])? $page["page_keywords"]:(isNotEmpty($page["page_content"])? getKeywordsFromContent ($page["page_content"]):$defDescription));
		$created = (isset($page['page_created']) && $page['page_created'] !== "0000-00-00 00:00:00"? $page['page_created']:(isset($page['page_modified']) && $page['page_modified'] !== "0000-00-00 00:00:00"? $page['page_modified']:date('Y-m-dTH:i:s'))); $created = str_replace(' ', 'T', $created);
		$modified = (isset($page['page_modified']) && $page['page_modified'] !== "0000-00-00 00:00:00"? $page['page_modified']:(isset($page['page_created']) && $page['page_created'] !== "0000-00-00 00:00:00"? $page['page_created']:date('Y-m-d H:i:s'))); $modified = str_replace(' ', 'T', $modified);
		$mainCls = $page["page_style"];
		require_once("includes/header.php");
?>

<?php echo html_entity_decode(captchaReplace($page["page_content"])); ?>
	
<?php require_once("includes/footer.php");
	}
?>
