<?php
//error_reporting(E_ALL);
error_reporting(0);
require_once('../../wpn-cms/init.php');
$siteid = 29;
$lang = changeLang("en");
$slug = getSlug();
$return = (isset($_GET["return"])? $_GET["return"]:(isset($_GET["simple"])? "simple":"all"));
$format = (isset($_GET["format"])? $_GET["format"]:"html5");
ob_start("sanitize_output_sm");//sanitize_output

if (isset($_GET["lang"])){ 
	$lang = changeLang($_GET["lang"]);
}else if (isset($_COOKIE["lang"])){
	$lang = changeLang($_COOKIE["lang"]);
}else{ 
	$lang = changeLang("en");
}

if ((($page = getPageBySlug($slug)) !== false) || (($page = getPageBySlug("/404")) !== false)){	
	$lang = changeLang($page["page_lang"]);	
	include_once("templates/". $page["page_template"] . ".php");	
}else{
	include_once("templates/404.php");
}
		
?>